package com.modal;

import javax.persistence.*;

@Entity
public class Book implements LoanableItem{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Long Id;
	protected String author;
	protected String name;
	protected String isbn;
	protected boolean bestSeller;
	
	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public boolean isBestSeller() {
		return bestSeller;
	}

	public void setBestSeller(boolean bestSeller) {
		this.bestSeller = bestSeller;
	}

	@Override
	public String toString() {
		return "Book [Id=" + Id + ", author=" + author + ", name=" + name + ", isbn=" + isbn + ", bestSeller="
				+ bestSeller + "]";
	}
	
	
	
}
