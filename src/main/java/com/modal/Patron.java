package com.modal;

import java.time.LocalDate;

public class Patron {
	protected String name;
	protected String address;
	protected String phoneNumber;
	protected String libraryCardNumber;
	protected LocalDate birthDate;
	protected Integer age;

}
