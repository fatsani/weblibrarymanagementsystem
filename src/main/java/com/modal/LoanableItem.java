package com.modal;

import java.time.LocalDate;

public interface LoanableItem extends Item {
	Money perDayFine = null;
	LocalDate checkOutPeriod = null; 
	Money value = null;
	
}
