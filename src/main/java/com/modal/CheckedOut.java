package com.modal;

import java.time.LocalDate;

public class CheckedOut {
	protected boolean hasBeenRenewed = false;
	protected LocalDate dueDate;
	protected Money fine;
	protected LocalDate whenReturned;

}
