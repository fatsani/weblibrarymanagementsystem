package com.repository;

import org.springframework.data.repository.CrudRepository;
import java.util.List;
import com.modal.Book;

public interface LibraryRepository extends CrudRepository<Book, Long> {
	List<Book> findByAuthor(String author);
}
