package com.repos;

import org.springframework.data.repository.CrudRepository;

import com.modal.Book;

public interface BookRepository extends CrudRepository<Book, Long> {

}
