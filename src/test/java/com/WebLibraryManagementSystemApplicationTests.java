package com;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.modal.Book;
import com.repos.BookRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WebLibraryManagementSystemApplicationTests {

	@Autowired
	private BookRepository bookRepository;
	
	@Test
	public void createBookTest() {
		Book book = new Book();
		book.setAuthor("Fatsani Phiri");
		book.setName("UnderAchievers");
		book.setIsbn("X1234");
		book.setBestSeller(true);
		bookRepository.save(book);
	}
}
